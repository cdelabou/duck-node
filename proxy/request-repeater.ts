import ByteBuffer from "../util/buffer"
import RemoteObject from "../remote-object"
import { LOGGER } from "../util/logger"
import PromiseState from "./promise-state"
import DatagramHandler from "../network/datagram-handler";
import RemoteError from "../remote-error";


/**
 * Thread performing request repetition when the promise is not complete after some amount of time
 */
export default class RequestRepeater {
    tryCount = 0;

    constructor(
        private callId: number,
        private remoteObject: RemoteObject,
        private messageByteBuffer: ByteBuffer,
        private promise: PromiseState
    ) {
        setTimeout(this.retry.bind(this), 500)
        
    }


    retry() {
        // Check if still waiting for answer
        if (this.promise.done) return;
        
        this.tryCount += 1;

        if (this.tryCount < 4) {
            // Try again
            LOGGER.debug(`request ${this.callId} was not answered yet, sending request again (retry ${this.tryCount})`)
            DatagramHandler.send(
                this.callId,
                this.messageByteBuffer,
                this.remoteObject.identifier.ip,
                this.remoteObject.identifier.port,
                this.tryCount // attempt number
            )

            setTimeout(this.retry.bind(this), 1000);
        } else {
            // Final timeout
            LOGGER.debug(`request ${this.callId} was not answered yet, giving up`)

            this.promise.reject(new RemoteError(
                `RemoteError`,
                `unable to fulfill request, the server is not answering`
            ));
        }
    }
}