import ByteBuffer from "../util/buffer";

export default class PromiseState {
	public done = false;

	constructor(
		private resolveFn: (buffer: ByteBuffer | null) => void,
		private rejectFn: (err: Error) => void
	) {}

	reject(error: Error) {
		this.done = true;
		this.rejectFn(error)
	}

    resolve(result: ByteBuffer | null) {
		this.done = true;
		this.resolveFn(result);
    }
}