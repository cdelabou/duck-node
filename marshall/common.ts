export const NULL = -128
export const ESCAPE = -127

export class MarshallingError extends Error {}


/**
 * Create kotlin type for given non generic type, either array with give depth or the class type
 */
export function typeOf(type: RawType | Function, arrayDepth: number = 0, nullable: boolean = false): DataType {
    return { type, arrayDepth, nullable };
}

export enum RawType {
    LONG,
    INT,
    DOUBLE,
    FLOAT,
    STRING,
    UNDEFINED,
    BYTES,
    BOOLEAN
}

export interface DataType {
    type: RawType | Function,
    arrayDepth: number,
    nullable: boolean
}