import { promises as dns } from "dns";
import { ByteBuffer } from ".";

/**
 * Unique identifier for a remote object
 */
export default class RemoteObjectIdentifier{
    private constructor(public code: number, public ip: string, public port: number) {}

    rawIp() {
        if (this.ip.includes(".")) {
            return new Uint8Array(this.ip.split(".").map(parseFloat));
        } else {
            return new Uint8Array(this.ip.split(":").map(parseFloat));
        }
    }

    toString(): string {
        return `{ code=${this.code}, ip=${this.ip}, port=${this.port} }`
    }

    static async of(code: number, ip: string, port: number) {
        // Resolve ip
        ip = (await dns.lookup(ip)).address;
        return new RemoteObjectIdentifier(code, ip, port);
    }

    static ofBytes(code: number, ip: Uint8Array | ByteBuffer, port: number) {
        ip = ip instanceof ByteBuffer ? ip.buffer : ip;

        if (ip.length === 4) {
            return new RemoteObjectIdentifier(code, ip.join("."), port);
        } else {
            return new RemoteObjectIdentifier(code, ip.join(":"), port);
        }
    }
}