import { MessageType, messageHandlers } from "./message-types"
import ByteBuffer from "../util/buffer";
import marshall from "../marshall/marshalling";


/**
 * Object handling the incoming messages from UDP packets.
 *
 * It calls the appropriate method, getter or setter of the local remote objects.
 * In case the message received is a answer or an error, it should return the the proxy thread
 * using the promise saved (@see Promise).
 *
 * All these behaviors are defined in the different message types (@see MessageType).
 *
 */
class SkeletonClass {
    private requestResults: {[key: string]: ByteBuffer} = {}

    /**
     * Handle an incoming message
     */
    async handle(callId: number, localCallId: string, buffer: ByteBuffer): Promise<ByteBuffer | null> {
        // Many message types
        const messageType = buffer.get()

        // Repeat response if it was stored earlier (in SET or CALL behavior)
        if (localCallId in this.requestResults) {
            return ByteBuffer.concat(
                ByteBuffer.alloc(1).putByte(MessageType.ANSWER),
                this.requestResults[localCallId]
            )
        }

        try {
            const answer = await messageHandlers[messageType](buffer, callId, localCallId);

            if (answer) {
                // Save result if necessary
                if (answer.save) {
                    this.requestResults[localCallId] = answer.data
                }

                // Append message type and return
                return ByteBuffer.concat(ByteBuffer.alloc(1).putByte(MessageType.ANSWER), answer.data)
            }
        } catch (e) {
            // Exception : send exception message
            const exceptionType = marshall(e.name)
            const message = marshall(e.toString())

            return ByteBuffer.concat(
                ByteBuffer.alloc(1).putByte(MessageType.ERROR),
                exceptionType,
                message
            );
        }

        return null;
    }
}


// Instanciate the skeleton
const Skeleton = new SkeletonClass(); 
export default Skeleton