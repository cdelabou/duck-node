import RemoteObjectIdentifier from "./remote-object-identifier"
import duckProperties from "./properties"
import ByteBuffer from "./util/buffer"
import unmarshall from "./marshall/unmarshalling"
import { typeOf, RawType } from "./marshall/common"
import { RemoteInterfaceData } from "./annotations"


/**
 * Remote objects base class, provide also many static methods to handle remote objects
 */
export default abstract class RemoteObject {
    constructor(public identifier: RemoteObjectIdentifier) {
        // Register object if not stub
        if (!Object.getPrototypeOf(this).hasOwnProperty("duckRemoteStub")) {
            // Save into local remote object list
            RemoteObject.registered[identifier.code] = this
        }
    }

    equals(other: any): boolean {
        return other instanceof RemoteObject ? this.identifier == other.identifier : false
    }

    /**
     * Static part for manging remote objects
     */
    private static idIndex = 0

    static registered: { [key: number]: RemoteObject } = {};
    static factories: { [className: string]: ((id: RemoteObjectIdentifier) => RemoteObject) } = {}

    /**
     * Generate a new local identifier for a remote object hosted here
     */
    static localIdentifier() {
        while (this.registered[this.idIndex] != null) this.idIndex++
        return RemoteObjectIdentifier.ofBytes(
            this.idIndex,
            duckProperties.localAddress,
            duckProperties.localPort
        )
    }

    /**
     * Return the nearest @RemoteInterface among superclasses, the @RemoteInterface parent should
     * contains annotations for @RemoteMember giving information about members id
     */
    static interfaceType(clazz: new () => RemoteObject): new () => RemoteObject {
        let current = clazz

        while (!(current as any).isRemoteInterface && current.name != RemoteObject.name) {
            current = Object.getPrototypeOf(current);
        }

        return current
    }

    /**
     * Returns the metadata of the super remote interface of this class
     * @param clazz class to analyse
     */
    static interfaceData(clazz: new () => RemoteObject): RemoteInterfaceData {
        return (this.interfaceType(clazz) as any).remoteInterfaceData;
    }

    /**
     * Create a object of the same super remote interface type, which should be a stub of a
     * remote object (ie. calling a method on the object shall call a method on the remote
     * object)
     */
    static stub<T extends RemoteObject> (id: RemoteObjectIdentifier, clazz: Function): T {
        let current = clazz

        while (this.factories[current.name] == null && current.name != RemoteObject.name) {
            current = Object.getPrototypeOf(current);
        }

        if (this.factories[current.name] == null) {
            throw Error(`unable to init instance of ${clazz.name}, no provider`)
        }

        return this.factories[current.name](id) as T
    }

    /**
     * Retrieve a local remote object from buffer
     */
    static fromByteBuffer(buffer: ByteBuffer): RemoteObject {
        // Call parameters
        const identifier = unmarshall<RemoteObjectIdentifier>(buffer, typeOf(RemoteObject))
        return this.registered[identifier.code]!
    }
}