import duckProperties from "./properties";
import { LOGGER, LogLevel } from "./util/logger"
import { parseArgs } from './util/console-util'
import ByteBuffer from './util/buffer'
import DatagramHandler from "./network/datagram-handler";
import { RemoteInterface, RemoteStub, DataClass } from "./annotations";
import RemoteObject from "./remote-object";
import RemoteObjectIdentifier from "./remote-object-identifier";
import { typeOf, RawType } from "./marshall/common";
import { Proxy } from "./proxy/proxy";

/**
 * Exports necessary tools for outside packages
 */
export {
	// Settings
	duckProperties,

	// Logging
	LOGGER,
	LogLevel,

	// Bytebuffer and utils
	ByteBuffer,
	DatagramHandler,
	parseArgs,
	typeOf,
	RawType,

	// Remote interface, objects
	RemoteInterface,
	RemoteObject,
	RemoteObjectIdentifier,
	RemoteStub,
	DataClass,

	// Proxy
	Proxy
};