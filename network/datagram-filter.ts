/**
 * Filter interface to intercept message before they are sent.
 * Returns a boolean indicating wheter this message should be sent
 */
type DatagramFilter = (message: Buffer, destination: string, port: number) => boolean;

export default DatagramFilter;